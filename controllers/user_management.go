package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"golang-first-project/models"
	"net/http"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	inputUserID := params["userId"]
	var users models.Users

	// Get user
	models.GetDB().First(&users, inputUserID)

	json.NewEncoder(w).Encode(users)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	json.NewDecoder(r.Body).Decode(&users)

	// Create new user
	models.GetDB().Create(&users)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	var users models.Users
	json.NewDecoder(r.Body).Decode(&users)

	// Update user
	models.GetDB().Save(&users)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}


func DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	inputID := params["userId"]
	var users models.Users

	// Delete user
	models.GetDB().Where("id = ?", inputID).Delete(&users)

	w.WriteHeader(http.StatusNoContent)
}
