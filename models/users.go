package models

// User структура пользователя
type Users struct {
	ID int `json:"Id" gorm:"primary_key"`
	Name string `json:"Name"`
}
