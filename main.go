package main

import (
	"github.com/gorilla/mux"
	"golang-first-project/controllers"
	_ "golang-first-project/controllers"
	_ "golang-first-project/models"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	// Get user
	router.HandleFunc("/users/{userId}", controllers.GetUser).Methods("GET")
	// Create user
	router.HandleFunc("/users/create", controllers.CreateUser).Methods("POST")
	// Update user
	router.HandleFunc("/users/{userId}", controllers.UpdateUser).Methods("PUT")
	// Delete user
	router.HandleFunc("/users/{userId}", controllers.DeleteUser).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8080", router))
}
